import pandas as pd

# Načtení Excel souboru
df = pd.read_excel('means-kotara.xlsx')
print(df.columns)

# Výpočet průměru ze sloupce H bez nulových hodnot
average = df['Hodnocení této verze:'][df['Hodnocení této verze:'] != 0].mean()

# Vypsání průměru
print("Průměr je: ", average)
