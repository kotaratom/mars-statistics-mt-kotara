from scipy.stats import pearsonr

# předpokládáme, že máme dva seznamy dat
polozka_1 = [3.65, 3.00, 2.78 ]
polozka_2 = [3.55, 2.98, 2.95 ]

# spočítáme Pearsonovu korelaci
corr, p_value = pearsonr(polozka_1, polozka_2)

# vypíšeme výsledek
print("Pearsonova korelace: ", corr)
print("p-hodnota: ", p_value)
