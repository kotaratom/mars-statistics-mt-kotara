import pingouin as pg
import pandas as pd

# Vytvoření matici dat
data = [[1, 2, 3, 4, 5],
        [5, 4, 3, 2, 1],
        [1, 2, 1, 2, 1],
        [4, 4, 4, 4, 4]]

df = pd.DataFrame(data, columns=['Item1', 'Item2', 'Item3', 'Item4', 'Item5'])

# Výpočet Cronbachova alfa
alpha = pg.cronbach_alpha(data=df, items=['Item1', 'Item2', 'Item3', 'Item4', 'Item5'])

print("Cronbach's alpha:", alpha)
