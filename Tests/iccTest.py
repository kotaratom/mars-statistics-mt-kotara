import pingouin as pg
import pandas as pd

#create DataFrame
df = pd.DataFrame({'exam': [1, 2, 3, 4, 5, 6,
                            1, 2, 3, 4, 5, 6],
                   'judge': ['A', 'A', 'A', 'A', 'A', 'A',
                             'B', 'B', 'B', 'B', 'B', 'B'],
                   'rating': [1, 1, 3, 6, 6, 7,
                              0, 4, 1, 5, 5, 6,]})

#view first five rows of DataFrame
df.head()

icc = pg.intraclass_corr(data=df, targets='exam', raters='judge', ratings='rating')

icc.set_index('Type')

print(icc)
