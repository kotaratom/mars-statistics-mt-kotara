import numpy as np
import pandas as pd
import mplfinance as mpf
import matplotlib.pyplot as plt


# Funkce pro výpočet hodnoty H
def h(c, x, y):
    """
    Vypočítá hodnotu H pro dané korelace mezi položkami a dvě skupiny položek.

    Parametry:
    - c: korelace mezi položkami i a j
    - x: korelace položky i s ostatními položkami
    - y: korelace položky j s ostatními položkami

    Výstup:
    - Hodnota H pro položky i a j
    """
    n = len(x)
    r = np.sum(x > c) + np.sum(y > c)
    return (2 * r) / (n * (n - 1))


# Načtení dat
np.random.seed(42)  # zajistí stejná data při každém spuštění
datasets = []
for i in range(5):
    datasets.append(np.random.normal(0, 1, size=(100, 20)))

# Výpočet korelací mezi položkami a hodnot H
H = []
for dataset in datasets:
    corr = np.corrcoef(dataset.T)
    n_items = corr.shape[0]
    H_i = np.zeros((n_items, n_items))
    for i in range(n_items):
        for j in range(i + 1, n_items):
            H_i[i, j] = h(corr[i, j], corr[i, :], corr[j, :])
    H.append(H_i[H_i > 0])

# Vytvoření box plot diagramu pro hodnoty faktorů
fig, axs = plt.subplots(1, 5, figsize=(12, 4), sharey=True)

for i, ax in enumerate(axs.flat):
    ax.boxplot(H[i])
    ax.set_title(f'Dataset {i + 1}')
    ax.set_ylabel('Hodnota H')

plt.show()

# # Vytvoření scatter plotu pro jednotlivé datasety
# fig, axs = plt.subplots(1, 5, figsize=(12, 4), sharey=True)
#
# for i, ax in enumerate(axs.flat):
#     ax.scatter(X[i][:, 0], X[i][:, 1], s=10, c='skyblue', alpha=0.5)
#     ax.set_title(f'Dataset {i + 1}')
#     ax.set_xlabel('Proměnná 1')
#     ax.set_ylabel('Proměnná 2')
#
# plt.show()

