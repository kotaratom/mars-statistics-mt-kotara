import pandas as pd
from scipy.stats import pearsonr

# načtení dat z Excelu
df = pd.read_excel('nazev_souboru.xlsx', sheet_name='nazev_listu')
# výpočet korelace
corr = df['nazev_sloupce1'].corr(df['nazev_sloupce2'])
# výpočet p-hodnoty
corr, p_value = pearsonr(df['nazev_sloupce1'], df['nazev_sloupce2'])
# tisk výsledků
print(f"Korelace: {corr}")
print(f"P-hodnota: {p_value}")
