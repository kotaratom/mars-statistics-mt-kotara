import pandas as pd
import pingouin as pg
from openpyxl import load_workbook
import matplotlib.pyplot as plt

# Nahraďte 'your_file.xlsx' cestou k vašemu Excel souboru
file_path = 'statMarsScore.xlsx'

# Přečtěte si Excel soubor do pandas DataFrame
df = pd.read_excel(file_path, engine='openpyxl')

# Nahraďte desetinné tečky čárkami
df['MARS Score'] = df['MARS Score'].apply(lambda x: str(x).replace('.', ','))

# Změňte desetinnou čárku zpět na desetinnou tečku pro výpočty
df['MARS Score'] = df['MARS Score'].apply(lambda x: float(x.replace(',', '.')))

icc = pg.intraclass_corr(data=df, targets='Vzorek ID', raters='Platform ID', ratings='MARS Score', nan_policy='omit').round(3)
icc.set_index("Type", inplace=True)
print(icc.loc["ICC2"])

# Vytvořte graf s ICC hodnotami
fig, ax = plt.subplots()
ax.bar(icc.index, icc["ICC"])
ax.set_ylim(0, 1)
ax.set_xlabel("ICC Typ")
ax.set_ylabel("ICC hodnota")
ax.set_title("Intraclass Correlation Coefficient (ICC)")

plt.show()