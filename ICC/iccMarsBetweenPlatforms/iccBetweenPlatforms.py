import pandas as pd
import pingouin as pg
from openpyxl import load_workbook
import matplotlib.pyplot as plt

# Nahraďte 'your_file.xlsx' cestou k vašemu Excel souboru
file_path = 'iccMeziPlatformami.xlsx'

# Přečtěte si Excel soubor do pandas DataFrame
df = pd.read_excel(file_path, engine='openpyxl')

iccA = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateA')
iccB = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateB')
iccC = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateC')
iccD = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateD')
iccE = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateE')
iccF = pg.intraclass_corr(data=df, targets='App', raters='Platform', ratings='RateF')
iccComplete = pg.intraclass_corr(data=df, targets='AppComplete', raters='PlatformComplete', ratings='RateComplete')

print("ICC A")
print(iccA)
print("ICC B")
print(iccB)
print("ICC C")
print(iccC)
print("ICC D")
print(iccD)
print("ICC E")
print(iccE)
print("ICC F")
print(iccF)

print("ICC Complete all apps")
print(iccComplete)


