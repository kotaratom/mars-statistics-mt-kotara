import pandas as pd
import pingouin as pg

excel_soubor = 'data-hodnotitel-AfinalICC.xlsx'
sheet_name = 'SourceData'

df = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100)

print(df)

icc = pg.intraclass_corr(data=df, targets='ID', raters='ID platformy', ratings='Mean')
icc.set_index('Type')

print(icc)
