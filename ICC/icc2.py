import pandas as pd
import numpy as np
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
import seaborn as sns

# Data
data = [
    ["Adiquit: Přestaňte kouřit", "iPhone ;", 4.6],
    ["Adiquit: Přestaňte kouřit", "Android ;", 3.34],
    ["aktiBMI", "iPhone ;", 4.37],
    ["aktiBMI", "Android ;", 3],
    ["BENU aplikace", "Android ;", 3.27],
    ["BENU aplikace", "iPhone ;", 3],
    ["Brainbuddy: Quit Porn Forever", "Android ;", 3.34],
    ["Brainbuddy: Quit Porn Forever", "iPhone ;", 2.99],
    ["Deník hráče", "Android ;", 2.05],
    ["Deník hráče", "iPhone ;", 2.04],
    ["Dexcom Clarity", "Android ;", 2.77],
    ["Dexcom Clarity", "iPhone ;", 2.93],
    ["DWA: Počitadlo střízlivosti", "Android ;", 2.73],
    ["DWA: Počitadlo střízlivosti", "iPhone ;", 3.1],
    ["DWS: počítadlo bez kouře", "Android ;", 2.09],
    ["DWS: Počítadlo bez kouře", "iPhone ;", 2.42]
]

# Create DataFrame
df = pd.DataFrame(data, columns=["Jméno aplikace", "Platforma", "MARS Score"])

# Calculate ICC
iphone_scores = df[df["Platforma"] == "iPhone ;"]["MARS Score"].to_numpy()
android_scores = df[df["Platforma"] == "Android ;"]["MARS Score"].to_numpy()

icc, _ = pearsonr(iphone_scores, android_scores)

print("ICC:", icc)

# Plot
plt.figure(figsize=(10, 5))
sns.scatterplot(data=df, x="Jméno aplikace", y="MARS Score", hue="Platforma")
plt.xticks(rotation=90)
plt.title("MARS Score pro iPhone a Android")
plt.show()
