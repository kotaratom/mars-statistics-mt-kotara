import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error

data = {
    'PS': ['PS_Android', 'PS_Iphone'],
    'A': [3.16, 3.396078431],
    'B': [3.261111111, 3.593137255],
    'C': [3.296296296, 3.39869281],
    'D': [3.255555556, 3.390196078],
    'E': [3.022222222, 3.200980392],
    'F': [3.344444444, 3.297385621],
    'Celk': [3.379412, 3.379412]
}

df = pd.DataFrame(data)
df = df.drop(columns=['PS'])

icc_values = {}

for column in df.columns:
    msr = np.var(df[column].to_numpy(), ddof=1)
    mse = mean_squared_error([df.loc[0, column]], [df.loc[1, column]])
    icc = (msr - mse) / (msr + mse)
    icc_values[column] = round(icc, 3)

print("ICC pro jednotlivé sloupce:", icc_values)
