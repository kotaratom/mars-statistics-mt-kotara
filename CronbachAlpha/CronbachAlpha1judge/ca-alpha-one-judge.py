import pandas as pd
import pingouin as pg

excel_soubor = 'data-hodnotitel-Afinal.xlsx'
sheet_name = 'SourceData'

columns_to_load = ["A1", "A2", "A3", "A4", "A5", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "D1", "D2", "D3", "D4", "D5", "D6"]

columns_to_load_all_dimensions = ["A1", "A2", "A3", "A4", "A5", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "D1", "D2", "D3", "D4", "D5", "D6", "E1", "E2", "E4", "F1", "F2", "F3", "F4", "F5", "F6"]

df = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="AF:AJ")
df_B = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="AM:AP")
df_C = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="AS:AU")
df_D = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="AX:BC")
df_E = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="BH:BK")
df_F = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols="BN:BS")

df_total_mars = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols = columns_to_load)
df_mars_all_dimensions = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=50, usecols = columns_to_load_all_dimensions)

sectionA = pg.cronbach_alpha(data=df)
sectionB = pg.cronbach_alpha(data=df_B)
sectionC = pg.cronbach_alpha(data=df_C)
sectionD = pg.cronbach_alpha(data=df_D)
sectionE = pg.cronbach_alpha(data=df_E)
sectionF = pg.cronbach_alpha(data=df_D)

sectionTotalMARS = pg.cronbach_alpha(data=df_total_mars)
sectionMARS_all_dimensions = pg.cronbach_alpha(data=df_mars_all_dimensions)


print("Section A", sectionA)
print("Section B", sectionB)
print("Section C", sectionC)
print("Section D", sectionD)
print("Section E", sectionE)
print("Section F", sectionF)
print("Total MARS", sectionTotalMARS)
print("All dimensions", sectionMARS_all_dimensions)