import pandas as pd
import pingouin as pg

excel_soubor = 'data-hodnotitel-AplusBkor.xlsx'
sheet_name = 'SourceData'

columns_to_load = ["A1", "A2", "A3", "A4", "A5", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "D1", "D2", "D3", "D4", "D5", "D6"]

df = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols="AI:AM")
df_B = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols="AP:AS")
df_C = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols="AV:AX")
df_D = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols="BA:BF")
df_E = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols="BK:BN")

df_total_mars = pd.read_excel(excel_soubor, sheet_name=sheet_name, engine='openpyxl', nrows=100, usecols = columns_to_load)

sectionA = pg.cronbach_alpha(data=df)
sectionB = pg.cronbach_alpha(data=df_B)
sectionC = pg.cronbach_alpha(data=df_C)
sectionD = pg.cronbach_alpha(data=df_D)
sectionE = pg.cronbach_alpha(data=df_E)

sectionTotalMARS = pg.cronbach_alpha(data=df_total_mars)

print("Section A", sectionA)
print("Section B", sectionB)
print("Section C", sectionC)
print("Section D", sectionD)
print("Section E", sectionE)
print("Total MARS", sectionTotalMARS)

