import statistics

import pandas as pd
import pingouin as pg

# Nahraďte 'nazev_souboru.xlsx' názvem svého souboru
excel_soubor = 'data-hodnotitel-AplusBkor.xlsx'
sheet_nameA = 'A-rater'
sheet_nameB = 'B-rater'

# Načtení Excel souboru do DataFrame
dfAraterA = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="AI:AM")
dfBraterA = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="AI:AM")

dfAraterB = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="AP:AS")
dfBraterB = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="AP:AS")

dfAraterC = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="AV:AX")
dfBraterC = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="AV:AX")

dfAraterD = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="BA:BF")
dfBraterD = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="BA:BF")

dfAraterE = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="BK:BN")
dfBraterE = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="BK:BN")

dfAraterF = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=51, usecols="BQ:BV")
dfBraterF = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=51, usecols="BQ:BV")


caAraterSectionA = pg.cronbach_alpha(data=dfAraterA)
caBraterSectionA = pg.cronbach_alpha(data=dfBraterA)

caAraterSectionB = pg.cronbach_alpha(data=dfAraterB)
caBraterSectionB = pg.cronbach_alpha(data=dfBraterB)

caAraterSectionC = pg.cronbach_alpha(data=dfAraterC)
caBraterSectionC = pg.cronbach_alpha(data=dfBraterC)

caAraterSectionD = pg.cronbach_alpha(data=dfAraterD)
caBraterSectionD = pg.cronbach_alpha(data=dfBraterD)

caAraterSectionE = pg.cronbach_alpha(data=dfAraterE)
caBraterSectionE = pg.cronbach_alpha(data=dfBraterE)

caAraterSectionF = pg.cronbach_alpha(data=dfAraterF)
caBraterSectionF = pg.cronbach_alpha(data=dfBraterF)

columns_to_load = ["AI", "AJ", "AL", "AM", "AP", "AQ", "AR", "AS", "AV", "AW", "AX", "BA", "BB", "BC", "BD", "BE", "BF"]

df_total_marsA = pd.read_excel(excel_soubor, sheet_name=sheet_nameA, engine='openpyxl', nrows=100, usecols = "AI:AM,AP:AS,AV:AX,BA:BF,BK:BN")
sectionTotalMARSA = pg.cronbach_alpha(data=df_total_marsA)

df_total_marsB = pd.read_excel(excel_soubor, sheet_name=sheet_nameB, engine='openpyxl', nrows=100, usecols = "AI:AM,AP:AS,AV:AX,BA:BF,BK:BN")
sectionTotalMARSB = pg.cronbach_alpha(data=df_total_marsB)

print("caAraterSectionA", caAraterSectionA)
print("caBraterSectionA", caBraterSectionA)

print("caAraterSectionB", caAraterSectionB)
print("caBraterSectionB", caBraterSectionB)

print("caAraterSectionC", caAraterSectionC)
print("caBraterSectionC", caBraterSectionC)

print("caAraterSectionD", caAraterSectionD)
print("caBraterSectionD", caBraterSectionD)

print("caAraterSectionE", caAraterSectionE)
print("caBraterSectionE", caBraterSectionE)

print("caAraterSectionF", caAraterSectionF)
print("caBraterSectionF", caBraterSectionF)

print("Total MARS", sectionTotalMARSA)
print("Total MARS", sectionTotalMARSB)




